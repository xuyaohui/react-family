//创建仓库
import createStore from './createStore';
//将普通的actionCreator，变为可以dipatch(action)的actionCreator
import bindActionCreators from './bindActionCreators';
//合并多个reducer，将多个reducer的状态合并到一个状态树上
import combineReducers from './combineReducers';
import applyMiddleware from './applyMiddleware';
import compose from './compose';
export {
    createStore,
    bindActionCreators,
    combineReducers,
    applyMiddleware,
    compose
}