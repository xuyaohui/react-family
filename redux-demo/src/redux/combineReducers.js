/**
 * 合并多个reducer，得到新的reducer
 * 新的reducer会将每个reducer都执行一遍，返回新的state
    import counter1 from './counter1';
    import counter2 from './counter2';
    let reducer = combineReducers({
        counter1,
        counter2
    });
    最终得到的state的结构如下：
    {
        counter1:{number:10},
        counter2:{age:20}
    }
 */
export default function combineReducers(reducers){
    return (state={}, action) => {
        let newState = {};
        for(let key in reducers){
            newState[key] = reducers[key](state[key],action);
        }
        return newState;
    }
}