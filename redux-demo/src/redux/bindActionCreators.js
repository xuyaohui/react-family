/**
 * 改造前后actionCreator都是function：
 * 改造前：函数返回是一个action对象；
 * 改造后：函数直接执行了dispatch(action)
 */
// let actions = {
//     increment(val){//改造前的actionCreator
//         return {type:INCREMENT,payload:val};
//     },
//     _increment(val){
//         // 改造后的actionCreator
//         return store.dispatch({type:INCREMENT,payload:val});
//     }
// };
function bindActionCreator(actionCreators, dispatch){
    return (...args) => {
        dispatch(actionCreators.apply(this,args));
    }
}
export default function bindActionCreators(actionCreators, dispatch){
    if(typeof actionCreators === 'function'){
        return bindActionCreator(actionCreators, dispatch);
    }
    let obj = {};
    for(let key in actionCreators){
        const actionCreator = actionCreators[key];
        if (typeof actionCreator === 'function') {
            obj[key] = bindActionCreator(actionCreator, dispatch)
        }
    }
    return obj;
}