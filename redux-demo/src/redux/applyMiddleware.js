import compose from './compose';
let applyMiddleware = (...middlewares) => (createStore) => (reducer, preloadState) => {
    let store = createStore(reducer,preloadState);
    let dispatch = () => { throw new Error('dispatch is not ready')};
    let middlewareAPI = {
        getState: store.getState,
        dispatch: (...args) => dispatch(...args)
    };
    /**
     * chain的解构
     let chain = next => action =>{
        next(action);
    };
     */
    let chains = middlewares.map(middleware => middleware(middlewareAPI));
    dispatch = compose(...chains)(store.dispatch);
    return {
        ...store,
        dispatch
    }
}
export default applyMiddleware