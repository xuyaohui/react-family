import React from 'react';
import ReactDOM from 'react-dom';
// import App from './App';
import AppReactRedux from './App.react-redux';
ReactDOM.render(<AppReactRedux />, document.getElementById('root'));
