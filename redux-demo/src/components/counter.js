import React, { Component } from 'react';
import store from '../store';
import { bindActionCreators } from '../redux'
import * as counterAction from '../store/actions/counter';

let actions = bindActionCreators(counterAction,store.dispatch);

class Counter extends Component {
  constructor(){
      super();
      this.state = {
          number:store.getState().counter1.number,
          age:store.getState().counter2.age,
      }
  }
  add = () => {
    actions.increment(5);
  }
  minus = () => {
    actions.decrement(10);
  }
  add2 = () => {
    actions.increment2(5);
  }
  minus2 = () => {
    actions.decrement2(10);
  }
  render() {
    return (
      <div> 
          <p>计数器</p>
          <p> number:{ this.state.number}</p>
          <p> age:{ this.state.age}</p>
          <button onClick={this.add}>Number +5</button>
          <button onClick={this.minus}>Number -10</button>
          <button onClick={this.add2}>Age +5</button>
          <button onClick={this.minus2}>Age -10</button>
     </div>
    );
  }
  componentDidMount(){
      this.un = store.subscribe(()=>{
        this.setState({
            number: store.getState().counter1.number,
            age: store.getState().counter2.age,
        })
      });
  }
  componentWillUnmount(){
      this.un();
  }
}
export default  Counter