import React, { Component } from 'react';
import { connect } from '../react-redux';
import * as counterAction from '../store/actions/counter';
// import * as types from '../store/action-types';
class Counter extends Component {
  // constructor(){
  //   super();
  // }
  add = () => {
    this.props.increment(5);
  }
  minus = () => {
    this.props.decrement(10);
  }
  asyncAdd = () => {
    this.props.asyncIncrement(2);
  }
  promiseAdd = () =>{
    // this.props.promiseIncrement(4);
    this.props.promiseIncrement2(4);
  }
  add2 = () => {
    // this.props.increment2(5);
  }
  minus2 = () => {
    // this.props.decrement2(10);
  }
  render() {
    return (
      <div> 
          <p>计数器</p>
          <p> number:{ this.props.counter1.number}</p>
          <p> age:{ this.props.counter2.age}</p>
          <button onClick={this.add}>Number +5</button>
          <button onClick={this.minus}>Number -10</button>
          <button onClick={this.asyncAdd}>thunk,延时加2</button>
          <button onClick={this.promiseAdd}>promise,加4</button>
          {/* <button onClick={this.add2}>Age +5</button>
          <button onClick={this.minus2}>Age -10</button> */}
     </div>
    );
  }
  componentDidMount(){
    
  }
}
//将仓库的状态，映射到组件的props上
const mapStateToProps = (state) => {
  return {
    ...state
  };
};

//将所有的action，映射到组件的props上。
//此时react-redux内部对action做了一次封装(bindActionCreators)
// const mapDispatchToProps = (dispatch) => {
//   return {
//     increment(val){
//       dispatch({type: types.INCREMENT,payload:val});
//     },
//     decrement(val){
//       dispatch({type:types.DECREMENT,payload:val});
//     }
//   }
// };
// export default connect(mapStateToProps, mapDispatchToProps)(Counter);


export default connect(mapStateToProps, counterAction)(Counter);