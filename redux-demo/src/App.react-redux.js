import React, { Component } from 'react';
import Counter from './components/counter.react-redux';
import { Provider } from './react-redux';
import store from './store';
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
          <Counter />
      </Provider>
    );
  }
}
