/**
 * 用static contextType获取上下文对象
 */
import React from 'react';
import {bindActionCreators} from 'redux';
import {reduxContext} from './context';
function connect(mapStateToProps, mapDispatchToProps){
    return function(WrappedComponent){
        //最终导出的组件
        return class Proxy extends React.Component{
            //定义context
            static contextType = reduxContext
            constructor(props, context){
                //如果定义了上下文，则可以拿到context值
                super(props);
                this.state = mapStateToProps(context.store.getState());
            }
            render(){
                let actions = {};
                if(typeof mapDispatchToProps === 'function'){
                    actions = mapDispatchToProps(this.context.store.dispatch);
                }else{
                    actions = bindActionCreators(mapDispatchToProps,this.context.store.dispatch);
                }
                return <WrappedComponent {...this.state} {...actions}/>;
            }
            componentDidMount(){
                let store = this.context.store;
                this.unsubscribe = store.subscribe(()=>{
                    this.setState(mapStateToProps(store.getState()));
                });
            }
            componentWillUnmount(){
                this.unsubscribe();
            }
        };

    }
}
export default connect;