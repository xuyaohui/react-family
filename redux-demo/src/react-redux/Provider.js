import React, { Component } from 'react';
import {reduxContext} from './context';
export default class Provider extends Component {
  render() {
    return (
      <reduxContext.Provider value={{store:this.props.store}}>
          { this.props.children }
      </reduxContext.Provider>
    );
  }
}
