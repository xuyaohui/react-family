function createThunkMiddleware(extraArgument){
    return ({dispatch,getState}) => next => action =>{
        if(typeof action === 'function'){
            return action(dispatch, getState, extraArgument);
        }else{
            next(action);
        }
    }
}
let thunk = createThunkMiddleware();
//给中间件传递额外的参数extraArgument
thunk.withExtraArgument = createThunkMiddleware;
export default thunk;