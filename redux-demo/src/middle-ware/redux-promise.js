/**
 * 
 */
function isPromise(obj){
    return !!obj && typeof obj === 'object' && typeof obj.then === 'function';
}
export default ({dispatch, getState}) =>  next => action =>{
    if(isPromise(action)){
        return action.then(dispatch);
    }else{
        if(isPromise(action.payload)){
            return action.payload.then((result)=>{
                dispatch({...action,payload: result});
            }).catch(err=>{
                dispatch({...action,payload: err, error:true});
                //返回一个reject的promise
                return Promise.reject(err);
            });
        }else{
            return next(action);
        }
    }
}