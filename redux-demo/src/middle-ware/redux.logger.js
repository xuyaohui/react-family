/**
 * logger中间件
 */
let logger = ({dispatch,getState}) => next => action =>{
    console.log('旧状态',getState());
    next(action);
    console.log('新状态',getState());
};
export default logger;