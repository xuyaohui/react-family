/**
 * actionCreators,生成actions
 */
import {INCREMENT, DECREMENT, INCREMENT2, DECREMENT2} from '../action-types';
function increment(val){
    return {type:INCREMENT,payload:val};
}
function decrement(val){
    return {type:DECREMENT,payload:val};
}

function increment2(val){
    return {type:INCREMENT2,payload:val};
}
function decrement2(val){
    return {type:DECREMENT2,payload:val};
}
//thunk中间件，返回一个function
function asyncIncrement(val){
    return (dispatch, getState) => {
        setTimeout(()=>{
            dispatch({type:INCREMENT,payload:val})
        },1000);
    }
}

//promise，第一种写法，payload是一个promise
function promiseIncrement(val){
    return {
        type: INCREMENT,
        payload: new Promise((resolve, reject)=>{
            setTimeout(()=>{
                reject(val);
            },1000);
        })
    }
}
//promise的第二种写法，返回一个promise
function promiseIncrement2(val){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve({type:INCREMENT,payload:val});
        },1000);
    });
}
export {
    increment,
    decrement,
    increment2,
    decrement2,
    asyncIncrement,
    promiseIncrement,
    promiseIncrement2
};