import { createStore, applyMiddleware, compose} from '../redux';
import reducers from './reducers';

// import { applyMiddleware}  from 'redux';

// let store = createStore(reducers);

// let old_dispatch = store.dispatch;
// store.dispatch = function(action){
//     // do sth before...
//     old_dispatch(action);
//     // do sth after...
// }
//调试用
// window.store = store;

// import logger from '../middle-ware/redux.logger';
// let logger1 = store => {
//     return function(next){
//         return action =>{
//             console.log('logger1 before',store.getState());
//             next(action);
//             console.log('logger1 after',store.getState());
//         };
//     }
// }
// let logger2 = store => {
//     return function(next){
//         return action =>{
//             console.log('logger2 before',store.getState());
//             next(action);
//             console.log('logger2 after',store.getState());
//         };
//     }
// }
// let logger3 = store => {
//     return function(next){
//         return action =>{
//             console.log('logger3 before',store.getState());
//             next(action);
//             console.log('logger3 after',store.getState());
//         };
//     }
// }


// let store = applyMiddleware(logger1,logger2, logger3)(createStore)(reducers);

import thunk from '../middle-ware/redux-thunk';
import logger from '../middle-ware/redux.logger';
import reduxPromise from '../middle-ware/redux-promise';
let store = applyMiddleware(logger,thunk, reduxPromise)(createStore)(reducers);
export default store;