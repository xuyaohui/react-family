import {DECREMENT2,INCREMENT2} from '../action-types';
const initialState = {
    age: 20
}

export default (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT2:
            return {...state,age: state.age +action.payload};
        case DECREMENT2:
            return {...state,age: state.age -action.payload};
        default:
            return state
    }
};
