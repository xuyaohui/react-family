import {DECREMENT,INCREMENT} from '../action-types';
const initialState = {
    number: 10
}

export default (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT:
            return {...state,number: state.number + action.payload};
        case DECREMENT:
            return {...state,number: state.number - action.payload};
        default:
            return state
    }
};
