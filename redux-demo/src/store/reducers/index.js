import { combineReducers} from '../../redux';
import counter1 from './counter1';
import counter2 from './counter2';

/**
 最终得到的state是如下结构：
 {
     counter1:{number:10},
     counter2:{age:20}
 }
 */
// function combineReducers(reducers){
//     return (state={}, action) => {
//         let newState = {};
//         for(let key in reducers){
//             newState[key] = reducers[key](state[key],action);
//         }
//         return newState;
//     }
// }
let reducers = combineReducers({
    counter1,
    counter2
});
export default reducers;